
// Local Maven repository, for the SPARQL Benchmark Tools
resolvers += "Local Maven Repository" at "file://" + Path.userHome.absolutePath + "/.m2/repository"

lazy val distMuRADemo = (project in file("."))
    .settings(
        organization := "fr.inria.tyrex",
        name := "muir-distmura-public",
        version := "0.1.0-SNAPSHOT",
        scalaVersion := "2.11.12",
        // Locally installed MuIR
        libraryDependencies += "fr.inria.tyrex" % "muir-core" % "0.1.0-distmura",
        // Spark (Provided: classpath will be set by Spark-Submit)
        libraryDependencies += "org.apache.spark" %% "spark-core" % "2.4.0" % Provided,
        libraryDependencies += "org.apache.spark" %% "spark-sql" % "2.4.0" % Provided
    )
