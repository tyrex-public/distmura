# Demonstration of distributed Mu-RA

## Distributed Mu-RA

### Requirements
This repository contains the code and instructions needed to reproduce the experiments of the paper "Distributed Evaluation of Graph Queries using
Recursive Relational Algebra".

This repository is written in Scala to be executed in a Spark 2.4 cluster.
It requires to be compiled and executed in a Java&nbsp;8 environment with Scala&nbsp;2.11.12.

* Java 8 (tested with [Eclipse Temurin 8](https://adoptium.net/?variant=openjdk8&jvmVariant=hotspot))
* [Spark 2.4.x](https://archive.apache.org/dist/spark/spark-2.4.5/)
* [Maven 3.x](https://maven.apache.org/)
* [sbt](https://www.scala-sbt.org/) 1.3.x
* Scala 2.11.12 (installed by sbt)

You can also use the Docker image we provide in the [docker](./docker/README.md) folder of this
repository to try the experiments locally.

### Datasets

All the datasets used for the experiments reported in the paper are summarized in the following tables.

| Real Dataset       | Description                            | Edges       | Nodes       |
|--------------------|--------------------------------------|------------|------------|
| [Yago](https://www.mpi-inf.mpg.de/departments/databases-and-information-systems/research/yago-naga/yago)  | YAGO semantic knowledge base        | 62,643,951 | 42,832,856 |
| [Epinions](https://icon.colorado.edu/#!/) | Epinions product ratings (2005) | 13,668,320 | 996,744    |
| [Wikitree](https://dl.acm.org/doi/10.1145/2700464) | Online genealogy dataset | 9,192,212  | 1,382,751  |
| [Coauth-M](https://icon.colorado.edu/#!/) | MAG Geology coauthor simplices | 5,120,762  | 1,256,385  |
| [Gottron](https://dl.acm.org/doi/10.1145/2487788.2488173) | Wikipedia words | 2,941,903  | 273,961    |
| [AcTree](https://www.nature.com/articles/s41467-018-07034-y) | Academic family tree data | 1,561,494  | 777,220    |
| [Wikitree_0]((https://dl.acm.org/doi/10.1145/2700464)) | Wikitree filtered on relation ID 0 | 1,556,453  | 1,019,438  |
| [Reddit](https://snap.stanford.edu/data/) | Hyperlinks between subreddits | 858,490    | 55,863     |
| [TW-Cannes](https://www.nature.com/articles/s41598-020-61523-z) | Cannes Multiplex social network | 991,855    | 438,539    |
| [Higgs-RW](https://icon.colorado.edu/#!/) | Twitter, Higgs boson (2012) | 733,647    | 425,008    |
| [Wikidata_c](https://www.wikidata.org/wiki/Wikidata:Main_Page) | Wikidata child relation | 280,405    | 333,572    |
| [Wikidata_p](https://www.wikidata.org/wiki/Wikidata:Main_Page)| Wikidata father & mother relations | 280,740    | 334,430    |
| [Facebook](https://snap.stanford.edu/data/) | Social circles from Facebook | 88,234     | 4,039      |
| [Ragusan](https://icon.colorado.edu/#!/) | Ragusan nobility genealogy | 51,938     | 13,690     |
| [Isle-of-Man](https://icon.colorado.edu/#!/) | Isle of Man genealogy | 36,666     | 10,474     |
| [Fr-Royalty](https://hal.science/hal-03402766) | French royalty genealogy tree | 12,358     | 2,127      |

#### Synthetic Datasets

| Synthetic Dataset  | Edges       | Nodes       |
|--------------------|------------|------------|
| UniProt 10M       | 10,001,920  | 10,000,000 |
| UniProt 5M        | 5,001,427   | 5,000,000  |
| UniProt 1M        | 1,000,443   | 1,000,000  |
| UniProt 100k      | 66,181      | 100,000    |
| Random 0.001 100k | 5,003,893   | 100,000    |
| Random 0.001 10k  | 249,791     | 10,000     |
| Random 0.001 7k   | 24,630      | 7,000      |
| Random 0.001 5k   | 12,660      | 5,000      |
| Tree 10k          | 9,999       | 10,000     |
| Tree 7k           | 6,999       | 7,000      |
| Tree 5k           | 4,999       | 5,000      |


All datasets can be downloaded here:
[datasets.zip](https://cloud.univ-grenoble-alpes.fr/index.php/s/pHQLy4G4HDiCcRk)
(1.3Gb to download, 4.2Gb uncompressed).
<br>
This archive contains the .csv files of the graphs used in the experiments.

You can download it with the following command:
```bash
wget https://cloud.univ-grenoble-alpes.fr/index.php/s/pHQLy4G4HDiCcRk/download/data.zip
unzip data.zip
```

**Note:** This is the same file that is used in the BigDatalog section

### Query files

All the queries used in the experiments can be found in the `queries/ucrpq_queries` folder.

* Queries Q1 to Q25 are related to the Yago dataset (`datasets/distmura_format/yago_graph.csv`).
* Queries Q26 to Q50 are related to the Uniprot dataset (`datasets/distmura_format/uniprot_*.csv`).

### Compilation

1. Install the MuIR DistMuRA JAR in your local Maven repository:
   ```bash
   mvn install:install-file -Dfile="muIR-assembly-0.1.0-SNAPSHOT.jar" \
       -DgroupId="fr.inria.tyrex" \
       -DartifactId="muir-core" \
       -Dversion="0.1.0-distmura" \
       -Dpackaging=jar
   ```
2. Compile the assembly JAR of this project. It will produce a ready-to-use JAR file in the
   `target/scala-2.11` folder, with the name matching `*-assembly-*.jar`. You should move this file
   up to the project root and rename it `distmura-demo-assembly.jar` (we'll use this same in the
   next commands):
   ```bash
   sbt assembly
   mv target/scala-2.11/muir-distmura-public-assembly-0.1.0-SNAPSHOT.jar distmura-demo-assembly.jar
   ```
3. Copy the assembly JAR to a place accessible by `spark-submit`
4. Also copy the whole `statLog` folder to the folder from where the `spark-submit` command will be 
   executed

#### A note about IDEs

* IntelliJ works well by loading the `build.sbt` file to make a new project
* If you use Eclipse:
  1. Call `sbt eclipse` to generate the project files
  2. Import the project into Eclipse
  3. Change the *Scala Compiler* properties of the Eclipse project to use Scala&nbsp;2.11 

### Usage

Below are commands to run each the different programs.
These commands require the following arguments:
* `$DATA_FILE`: Absolute path to the input dataset
* `$QUERY_PATH`: Path to a UCRPQ file. Queries used in the experiments are in queries/ucrpq_queries
* `$MASTER_URL`: Spark master URL. See [the Spark documentation](https://spark.apache.org/docs/2.4.5/submitting-applications.html#master-urls)
* `$NB_PARTITIONS`: Number of partitions in Spark. We set it to be the number of available cores in the cluster
* `$START_NODE`: Start vertex ID (used for the Reach queries)
 
#### UCRPQ programs

Executes Unions of Conjunctive Regular Path Queries.

* `$DATA_FILE` used in the experiments are `data/distmura_format/yago_graph.csv` (Q1-Q25) or
`datasets/distmura_format/uniprot_*.csv` (Q26-Q50)

```bash
spark-submit \
    --class fr.inria.tyrex.muIR.demo.UCRPQSparkTest \
    --driver-memory 40g \
    --conf spark.driver.maxResultSize=0 \
    distmura-demo-assembly.jar \
    $DATA_FILE \
    csv \
    --cluster \
    --mem 40g \
    --master $MASTER_URL \
    --partitions $NB_PARTITIONS \
    --query $QUERY_PATH \
    --rv 2 \
    --optim "1 1 1 0 0" \
    --mc
```

#### Non-regular language a<sup>n</sup>b<sup>n</sup>

Computes the pairs of nodes connected by a path composed of a number of edges labeled `a` followed
by  the same number of edges labeled `b`.

* `$APREDICATE`: Predicate label (`a`)
* `$BPREDICATE`: Predicate label (`b`)
```bash
spark-submit \
    --class fr.inria.tyrex.muIR.demo.NonRegularLanguageANBNTest \
    --driver-memory 40g \
    --conf spark.driver.maxResultSize=0 \
    distmura-demo-assembly.jar \
    $DATA_FILE \
    csv \
    --cluster \
    --mem 40g \
    --master $MASTER_URL \
    --partitions $NB_PARTITIONS \
    --rv 2 \
    --mc \
    --comment p+$APREDICATE+$BPREDICATE
```

#### Same Generation

Computes the pairs of nodes that are of the same generation in a graph.
 
* `$DATA_FILE` dataset file composed of two columns.`$DATA_FILE` used in the experiments are `datasets/distmura_format/*_sg.csv`. 

```bash
spark-submit \
    --class fr.inria.tyrex.muIR.demo.SameGenerationTest \
    --driver-memory 40g \
    --conf spark.driver.maxResultSize=0 \
    distmura-demo-assembly.jar \
    $DATA_FILE \
    csv \
    --cluster \
    --mem 40g \
    --master $MASTER_URL \
    --partitions $NB_PARTITIONS \
    --rv 2 \
    --mc
```


#### Filtered Same generation

computes the pairs of nodes that are of the same generation for a particular predicate `p`.

* `$PREDICATE`: Predicate label (`p`)

```bash
spark-submit \
    --class fr.inria.tyrex.muIR.demo.FilteredSameGenerationTest \
    --driver-memory 40g \
    --conf spark.driver.maxResultSize=0 \
    distmura-demo-assembly.jar \
    $DATA_FILE \
    csv \
    --cluster \
    --mem 40g \
    --master $MASTER_URL \
    --partitions $NB_PARTITIONS \
    --rv 2 \
    --mc \
    --optim "1 1 1 0 0" \
    --comment p+$PREDICATE
```

#### Joined Same generation

computes the pairs of nodes that are of the same generation for a particular set of predicates `P`.
* `$JOINDS_FILE`: Absolute path to the join dataset (`P`). Join datasets used in the experiments are
 `datasets/distmura_format/*_tojoin.csv`.

```bash
spark-submit \
    --class fr.inria.tyrex.muIR.demo.FilteredSameGenerationTest \
    --driver-memory 40g \
    --conf spark.driver.maxResultSize=0 \
    distmura-demo-assembly.jar \
    $JOINDS_FILE \
    csv \
    $DATA_FILE \
    csv \
    --cluster \
    --mem 40g \
    --master $MASTER_URL \
    --partitions $NB_PARTITIONS \
    --rv 2 \
    --mc \
    --optim "1 1 1 0 0" 
```

## Running the BigDatalog system

Please refer to link for the requirements and compilation of the system.

BigDatalog requires Java 8 to compile. It comes with its own compilation system.
If you have troubles building it, take a look at our [Docker recipe](docker/bigdatalog.Dockerfile).

You can also use the Docker image we provide in the [docker](./docker/README.md) folder of this
repository to try the experiments locally.

 ### Datasets

All the datasets used for the experiments (the same datasets as the ones used for Distmura but in a
format that  BigDatalog understands) can be downloaded here :
[data.zip](https://cloud.univ-grenoble-alpes.fr/index.php/s/pHQLy4G4HDiCcRk)
(1.3Gb to download, 4.2Gb uncompressed).

**Note:** This is the same file that is used in the Distributed Mu-RA part

### Query files

All the queries used in the experiments can be found in the `queries/datalog_query_programs` folder.

* Queries Q1 to Q25 are related to the Yago dataset (`datasets/bigdatalog_format/yago_graph.csv`).
* Queries Q26 to Q50 are related to the Uniprot dataset (`datasets/bigdatalog_format/uniprot_*.csv`).


### Usage

* `$MASTER_URL`: Spark master URL. See [the Spark documentation](https://spark.apache.org/docs/2.4.5/submitting-applications.html#master-urls)
* `$NB_PARTITIONS`: Number of partitions in Spark. We set it to be the number of available cores in the cluster
* `$BIGDATALOG_JAR`: Path to the samples JAR file of BigDatalog (`spark-examples-1.6.1-hadoop2.4.0.jar`)
* `$PROGRAM_FILE`: Path to a BigDatalog program (`.deal` file)
  Datalog programs used in the experiments are provided in 
  the `queries/datalog_query_programs` folder
* `$DATA_FILE`: Path to the input dataset file (in the BigDatalog format)
* `$START_NODE`: Start vertex ID (used for the Reach query)

```bash
spark-submit \
    --driver-memory 40g \
    --executor-memory 40g \
    --master $MASTER_URL \
    --conf spark.datalog.recursion.version=2 \
    --conf spark.datalog.uniondistinct.enabled=true \
    --conf spark.sql.shuffle.partitions=$NB_PARTITIONS \
    --conf spark.driver.maxResultSize=0 \
    --class org.apache.spark.examples.datalog.Experiments \
    $BIGDATALOG_JAR \
    --file=$(p=$PROGRAM_FILE; echo $p > ${p}.txt; echo ${p}.txt) \
    --queryform=$(grep "<-" $PROGRAM_FILE -m 1 | cut -d"<" -f1) \
    --program=99 \
    --checkpointdir="$(pwd)/tmp" \
    --baserelation_yago=$DATA_FILE \
    --baserelation_yagodup=$DATA_FILE \
    --baserelation_yagodupdup=$DATA_FILE 
```

#### Joined Same generation

* `$JOINDS_FILE`: Absolute path to the join dataset (`P`). Join datasets used in the experiments are
 `datasets/distmura_format/*_tojoin.csv`.

```bash
spark-submit \
    --driver-memory 40g \
    --executor-memory 40g \
    --master $MASTER_URL \
    --conf spark.datalog.recursion.version=2 \
    --conf spark.datalog.uniondistinct.enabled=true \
    --conf spark.sql.shuffle.partitions=$NB_PARTITIONS \
    --conf spark.driver.maxResultSize=0 \
    --class org.apache.spark.examples.datalog.Experiments \
    $BIGDATALOG_JAR \
    --file=$(p=$PROGRAM_FILE; echo $p > ${p}.txt; echo ${p}.txt) \
    --queryform=$(grep "<-" $PROGRAM_FILE -m 1 | cut -d"<" -f1) \
    --program=99 \
    --checkpointdir="$(pwd)/tmp" \
    --baserelation_yago=$DATA_FILE \
    --baserelation_yagodup=$JOINEDS_FILE \
    --baserelation_yagodupdup=$DATA_FILE 
```

## Comparing the different systems

In order to perform a comparison of the above systems, we run the given commands and get the
execution times from the Spark Web UI available at `${SPARK_URL}:8080`. 

You can also get the execution times by using `grep` when running a `spark-submit` command:
* Look for `EVAL TIME:` when running a Distributed Mu-RA computation
* Look for `execution time:` when running a Distributed Mu-RA computation

## Additional experiments
### Comparison between $\mathcal{P}_\texttt{plw}^\texttt{pg}$ and $\mathcal{P}_\texttt{plw}^\texttt{s}$
Queries used for comparison between the two systems on Kleene star expressions (see. figure 5) are found in [additional_experiments/distrPlansComparison.xlsx](additional_experiments/distrPlansComparison.xlsx) along with the sizes of those expressions.

### Cost model evaluation

To evaluate the cost model, we made measurements on 16 queries on Yago. For each query, we computed the estimated cost and corresponding evaluation time for all equivalent terms of the query.

All measurements are found in [additional_experiments/costModel.xlsx](additional_experiments/costModel.xlsx). 
Below are the results from three sample queries.
![truc](additional_experiments/costmodelCharts.png)

### Comparison with Myria
Since Myria is no longer maintained and its software stack has evolved, preventing deployment in a distributed setting, we were only able to test Myria on a single machine with four workers. In this setting, Myria could execute a subset of test queries
on small datasets. Results are found in [additional_experiments/myria.xlsx](additional_experiments/myria.xlsx). 
The charts below present the comparative performance results between Myria and
Dist-μ-RA for the SG queries and UniProt queries, respectively.

<img src="additional_experiments/myriaSG.png" alt="Alt text" width="500"/>
<img src="additional_experiments/myriaUniprot.png" alt="Alt text" width="500"/>