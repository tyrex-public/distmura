package fr.inria.tyrex.muIR.demo

import fr.inria.tyrex.muIR.core._

import scala.collection.immutable

/**
 * Computes the transitive closure on postgres
 *
 * Command line:
 * <pre>
 * variablePartInputGraph format [cstPartInputGraph format] --partitions p [--local locExec]
 * [--cluster] [-d] [-l] [-p] [--rc col] [--rv RecVer] [--mem mem] [--comment c]
 * </pre>
 * (see [[DemoUtil]] for more info on each of those options)
 *
 * If <code>cstPartInputGraph</code> and its format are not given, one input graph is
 * used for both variable part and constant part relations
 *
 * <strong>Note:</strong> Giving different files for variable and constant part allows to
 * have a different input format for each, or to compute a filtered
 * transitive closure by giving a cstPartInputGraph that contains
 * a subset of edges.
 */
object TransitiveClosureSetRDDTest {
    /**
     * Entry point
     *
     * @param args Command line arguments
     */
    def main(args: Array[String]): Unit = {
        val (testConfig, sparkExecutor) = DemoUtil.prepareExecution(args)

        val variable = RecVar("X", MuType(ColumnId("src"), ColumnId("trg")))
        // variable authors used in the computation of the variable part
        val path = testConfig.inputData.head.path
        val edges = RelVar(DemoUtil.getNameFromPath(path),
            MuType(ColumnId("src"), ColumnId("trg")),
            path, testConfig.inputData.head.format)
        // variable authors used in the computation of the constant part
        val cstPath = testConfig.inputData.last.path
        val cstEdges = RelVar(DemoUtil.getNameFromPath(cstPath),
            MuType(ColumnId("src"), ColumnId("trg")),
            cstPath, testConfig.inputData.last.format)


        val join_op1 = MuFactory.mkRenamedVar(edges,
            Renamings(immutable.HashMap(ColumnId("src") -> ColumnId("m"))))
        val join_op2 = MuFactory.mkRenamedVar(variable,
            Renamings(immutable.HashMap(ColumnId("trg") -> ColumnId("m"))))

        //A recursive term requiring several iterations:
        val baseline = MuFactory.mkDecMu(variable, MuFactory.mkVar(cstEdges),
            MuFactory.mkRemove(ColumnId("m"), MuFactory.mkJoin(join_op1, join_op2)))

        // Trigger the evaluation
        val recOptions = SetRDDOptions(repartitionCst = testConfig.repartitionCol)
        DemoUtil.runEvaluation(sparkExecutor, baseline, recOptions)
    }
}
