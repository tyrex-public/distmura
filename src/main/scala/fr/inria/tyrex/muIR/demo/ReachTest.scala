package fr.inria.tyrex.muIR.demo

import fr.inria.tyrex.muIR.core._

import scala.collection.immutable

/**
 * Computes Reach query
 *
 * Command line:
 * <pre>
 * inputGraph format --partitions p [--local locExec] [--cluster] [-d] [-l] [-p] [--rc col]
 * [--rv RecVer] [--mem mem] [--comment c]
 * </pre>
 * (see [[DemoUtil]] for more info on each of those options)
 */
object ReachTest {
    /**
     * Entry point
     *
     * @param args Command line arguments
     */
    def main(args: Array[String]): Unit = {
        val (testConfig, sparkExecutor) = DemoUtil.prepareExecution(args)

        val variable = RecVar("X", MuType(ColumnId("trg")))
        val path = testConfig.inputData.head.path
        val edges = RelVar(DemoUtil.getNameFromPath(path),
            MuType(ColumnId("src"), ColumnId("trg")),
            path, testConfig.inputData.head.format)
        // variable authors used in the computation of the constant part

        val join_op1 = MuFactory.mkRenamedVar(edges,
            Renamings(immutable.HashMap(ColumnId("src") -> ColumnId("m"))))
        val join_op2 = MuFactory.mkRenamedVar(variable,
            Renamings(immutable.HashMap(ColumnId("trg") -> ColumnId("m"))))

        // fixme hack
        val filter = testConfig.comment.split("\\+")(1) //.toInt
        println(s"filter $filter")
        // A recursive term requiring several iterations:
        val filtered = MuFactory.mkRemove(ColumnId("src"), MuFactory.mkFilter(MuFactory.mkVar(edges),
            Equal(ColumnId("src"), DataVal(filter))))

        val baseline = MuFactory.mkDecMu(variable, filtered, //MuFactory.mkVar(cstAuthors),
            MuFactory.mkRemove(ColumnId("m"),
                MuFactory.mkJoin(join_op1, join_op2)))

        // Trigger the evaluation
        val recOptions = SetRDDOptions(repartitionCst = testConfig.repartitionCol)
        DemoUtil.runEvaluation(sparkExecutor, baseline, recOptions)
    }
}
