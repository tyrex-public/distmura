package fr.inria.tyrex.muIR.demo

import fr.inria.tyrex.muIR.core._
import fr.inria.tyrex.muIR.eval._

import scala.collection.immutable


object FilteredSameGenerationTest {
  /**
    * Computes same generation nodes in a graph on Spark
    * Command line:
    *   variablePartInputGraph format [cstPartInputGraph format] --partitions p [--local locExec] [--cluster] [-d] [-l] [-p] [--rc col] [--rv RecVer] [--mem mem] [--comment c]
    *   (see test_util.scala for more info on each of those options)
    * if cstPartInputGraph and its format are not given, one input graph is
    * used for both variable part and constant part relations
    *
    * Note: Giving different files for variable and constant part allows to
    * have a different input format for each of them.
    */
  def main(args: Array[String]) {

    val testconfig = DemoUtil.parseArgs(args.toList, Conf())
    val le = testconfig.localExecutor match {
      case LocalWithMappings() => new LocalExecutorFactory(
        LocalExecutorConfig(true), new MappingsFactory())
      case LocalWithRows() => new LocalExecutorFactory(
        LocalExecutorConfig(true), new RowFactory())
      case PgSQL() => new PgSQLExecutorFactory(PgSQLConfig(showResult = false,
        distributed = testconfig.onCluster, showSQL = true))
    }
    val exec = new SparkExecutor(false,
      onCluster = testconfig.onCluster,
      appName = DemoUtil.getAppName(testconfig),
      partitions = testconfig.partitions,
      localExecutorFactory = le,
      master = testconfig.master)


    val variable = RecVar("X", immutable.Set(ColumnId("src"), ColumnId("predicate"), ColumnId("trg")))
    // variable parent used in the computation of the constant part
    val cstPath = testconfig.inputData.last.path
    val cstParent = RelVar(DemoUtil.getNameFromPath(cstPath),
      immutable.Set(ColumnId("src"), ColumnId("predicate"), ColumnId("trg")),
      cstPath, testconfig.inputData.last.format)
    //variable parent used in the computation of the variable part
    val parent = RelVar(DemoUtil.getNameFromPath(cstPath),
      immutable.Set(ColumnId("src"), ColumnId("predicate"), ColumnId("trg")),
      cstPath, testconfig.inputData.head.format)

    val siblings = MuFactory.mkRename(
      Renamings(immutable.HashMap(ColumnId("t1")-> ColumnId("src"), ColumnId("t2") -> ColumnId("trg"))),
      MuFactory.mkRemove(ColumnId("src"), MuFactory.mkJoin(
        MuFactory.mkRenamedVar(cstParent,
          Renamings(immutable.HashMap(ColumnId("trg") -> ColumnId("t1")))),
        MuFactory.mkRenamedVar(cstParent,
          Renamings(immutable.HashMap(ColumnId("trg") -> ColumnId("t2")))))))

    val join_op1 = MuFactory.mkRenamedVar(parent,
      Renamings(immutable.HashMap(ColumnId("src")->ColumnId("p1"),ColumnId("trg")->ColumnId("c1"))))
    val join_op2 = MuFactory.mkRemove(ColumnId("p2"),MuFactory.mkJoin(
        MuFactory.mkRenamedVar(variable,
          Renamings(immutable.HashMap(ColumnId("src") -> ColumnId("p1"), ColumnId("trg")->ColumnId("p2")))),
        MuFactory.mkRenamedVar(parent,
          Renamings(immutable.HashMap(ColumnId("src") -> ColumnId("p2"), ColumnId("trg")->ColumnId("c2"))))))

    val value = testconfig.comment.split("\\+").last // fixme hack
    val baseline = MuFactory.mkFilter(MuFactory.mkDecMu(variable, siblings,
      MuFactory.mkRename(Renamings(immutable.HashMap(ColumnId("c1")->ColumnId("src"), ColumnId("c2")->ColumnId("trg"))),
        MuFactory.mkRemove(ColumnId("p1"),
        MuFactory.mkJoin(
        join_op1,
        join_op2

        )))), Equal(ColumnId("predicate"), DataVal(value)))
    val opt = DemoUtil.bestTerm(Set(baseline), List(cstParent), testconfig.optimOptions.get)

    //trigger evaluation:
    val recOptions = DemoUtil.getRecOptions(testconfig)
    val pGen = new SparkPhysicalPlanGenerator(
      recOptions)

    pGen.generatePhysicalPlan(opt)
    val r = exec.evaluate(opt)

  }
}
