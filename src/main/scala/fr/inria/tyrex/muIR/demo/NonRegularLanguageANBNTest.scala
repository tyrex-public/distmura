package fr.inria.tyrex.muIR.demo

import fr.inria.tyrex.muIR.core._

import scala.collection.immutable

/**
 * Computes a^n^ b^n^ on a graph containing edges predicate labeled a or b on Spark
 * Command line:
 * <pre>
 * variablePartInputGraph format [cstPartInputGraph format] --partitions p
 * [--local locExec] [--cluster] [-d] [-l] [-p] [--rc col]
 * [--rv RecVer] [--mem mem] [--comment c]
 * </pre>
 * (see [[DemoUtil]] for more info on each of those options)
 *
 * If <code>cstPartInputGraph</code> and its format are not given, one input graph is
 * used for both variable part and constant part relations
 *
 * <strong>Note:</strong> Giving different files for variable and constant part allows to
 * have a different input format for each.
 */
object NonRegularLanguageANBNTest {
    /**
     * Entry point
     *
     * @param args Command line arguments
     */
    def main(args: Array[String]): Unit = {
        val (testConfig, sparkExecutor) = DemoUtil.prepareExecution(args)

        //A recursive term requiring several iterations:
        val data = testConfig.inputData
        val variable = RecVar("X", MuType(ColumnId("src"), ColumnId("trg")))
        val path = data.head.path
        val vAB = MuFactory.mkVar(RelVar(DemoUtil.getNameFromPath(path),
            immutable.Set(ColumnId("src"), ColumnId("predicate"), ColumnId("trg")),
            path, data.head.format))
        val labels = testConfig.comment.split("\\+") // fixme hack
        val aLabel = labels(1)
        val bLabel = labels(2)
        val vA = MuFactory.mkRemove(ColumnId("predicate"),
            MuFactory.mkFilter(vAB, Equal(ColumnId("predicate"), DataVal(aLabel))))
        val vB = MuFactory.mkRemove(ColumnId("predicate"),
            MuFactory.mkFilter(vAB, Equal(ColumnId("predicate"), DataVal(bLabel))))

        val cstPath = data.last.path
        val cstAB = MuFactory.mkVar(RelVar(DemoUtil.getNameFromPath(path),
            immutable.Set(ColumnId("src"), ColumnId("predicate"), ColumnId("trg")),
            cstPath, data.last.format))
        val cstA = MuFactory.mkRemove(ColumnId("predicate"),
            MuFactory.mkFilter(cstAB, Equal(ColumnId("predicate"), DataVal(aLabel))))
        val cstB = MuFactory.mkRemove(ColumnId("predicate"),
            MuFactory.mkFilter(cstAB, Equal(ColumnId("predicate"), DataVal(bLabel))))

        val join_op1 = MuFactory.mkRenamedVar(variable,
            Renamings(immutable.HashMap(ColumnId("src") -> ColumnId("m"), ColumnId("trg") -> ColumnId("n"))))
        val join_op2 = MuFactory.mkRename(
            Renamings(immutable.HashMap(ColumnId("trg") -> ColumnId("m"))),
            vA
        )
        val join_op3 = MuFactory.mkRename(
            Renamings(immutable.HashMap(ColumnId("src") -> ColumnId("n"))),
            vB
        )

        val cst = MuFactory.mkRemove(ColumnId("m"), MuFactory.mkJoin(
            MuFactory.mkRename(Renamings(immutable.HashMap(ColumnId("trg") -> ColumnId("m"))), cstA),
            MuFactory.mkRename(Renamings(immutable.HashMap(ColumnId("src") -> ColumnId("m"))), cstB)
        ))

        val baseline = MuFactory.mkDecMu(variable, cst,
            MuFactory.mkRemove(ColumnId("n"), MuFactory.mkRemove(ColumnId("m"),
                MuFactory.mkJoin(MuFactory.mkJoin(join_op1, join_op2), join_op3))))

        // Trigger the evaluation
        val recOptions = DemoUtil.getRecOptions(testConfig)
        DemoUtil.runEvaluation(sparkExecutor, baseline, recOptions)
    }
}
