package fr.inria.tyrex.muIR.demo

import fr.inria.tyrex.muIR.core._

import scala.collection.immutable

/**
 * Computes same generation nodes in a graph on Spark
 * Command line:
 * <pre>
 * variablePartInputGraph format [cstPartInputGraph format] --partitions p [--local locExec]
 * [--cluster] [-d] [-l] [-p] [--rc col] [--rv RecVer] [--mem mem] [--comment c]
 * </pre>
 * (see [[DemoUtil]] for more info on each of those options)
 *
 * If <code>cstPartInputGraph</code> and its format are not given, one input graph is
 * used for both variable part and constant part relations
 *
 * <strong>Note:</strong> Giving different files for variable and constant part allows to
 * have a different input format for each of them.
 */
object SameGenerationTest {
    /**
     * Entry point
     *
     * @param args Command line arguments
     */
    def main(args: Array[String]): Unit = {

        val (testConfig, sparkExecutor) = DemoUtil.prepareExecution(args)

        val variable = RecVar("X", immutable.Set(ColumnId("src"), ColumnId("trg")))
        // variable parent used in the computation of the constant part
        val cstPath = testConfig.inputData.last.path
        val cstParent = RelVar(DemoUtil.getNameFromPath(cstPath), immutable.Set(ColumnId("src"), ColumnId("trg")),
            cstPath, testConfig.inputData.last.format)
        //variable parent used in the computation of the variable part
        val parent = RelVar(DemoUtil.getNameFromPath(cstPath),
            immutable.Set(ColumnId("src"), ColumnId("trg")),
            cstPath, testConfig.inputData.head.format)

        val siblings = MuFactory.mkRename(
            Renamings(immutable.HashMap(ColumnId("t1") -> ColumnId("src"), ColumnId("t2") -> ColumnId("trg"))),
            MuFactory.mkRemove(ColumnId("src"), MuFactory.mkJoin(
                MuFactory.mkRenamedVar(cstParent,
                    Renamings(immutable.HashMap(ColumnId("trg") -> ColumnId("t1")))),
                MuFactory.mkRenamedVar(cstParent,
                    Renamings(immutable.HashMap(ColumnId("trg") -> ColumnId("t2")))))))

        val join_op1 = MuFactory.mkRenamedVar(parent,
            Renamings(immutable.HashMap(ColumnId("src") -> ColumnId("p1"), ColumnId("trg") -> ColumnId("c1"))))
        val join_op2 = MuFactory.mkRemove(ColumnId("p2"), MuFactory.mkJoin(
            MuFactory.mkRenamedVar(variable,
                Renamings(immutable.HashMap(ColumnId("src") -> ColumnId("p1"), ColumnId("trg") -> ColumnId("p2")))),
            MuFactory.mkRenamedVar(parent,
                Renamings(immutable.HashMap(ColumnId("src") -> ColumnId("p2"), ColumnId("trg") -> ColumnId("c2"))))))

        val baseline = MuFactory.mkDecMu(variable, siblings,
            MuFactory.mkRename(Renamings(immutable.HashMap(ColumnId("c1") -> ColumnId("src"), ColumnId("c2") -> ColumnId("trg"))),
                MuFactory.mkRemove(ColumnId("p1"),
                    MuFactory.mkJoin(
                        join_op1,
                        join_op2

                    ))))

        // Trigger evaluation
        val recOptions = DemoUtil.getRecOptions(testConfig)
        DemoUtil.runEvaluation(sparkExecutor, baseline, recOptions)
    }
}
