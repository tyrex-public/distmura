package fr.inria.tyrex.muIR.demo

import fr.inria.tyrex.muIR.core.{CentralRecOptions, CentralRecSetRDDOptions, DecRecOptions, DistRecOptions, Mu, MuPrinter, MuRewrite, MuRewriter, RelVar, SetRDDOptions}
import fr.inria.tyrex.muIR.costModel.MuCentralizedCost
import fr.inria.tyrex.muIR.eval._
import fr.inria.tyrex.muIR.optimizer.rules.OptimizerParams

import java.util.Properties
import java.util.concurrent.TimeUnit
import scala.annotation.tailrec

trait LocalExecConf

case class PgSQL() extends LocalExecConf

case class LocalWithRows() extends LocalExecConf

case class LocalWithMappings() extends LocalExecConf

case class InputData(path: String, format: String)

case class OptimOptions(swapAntiprojections: Boolean,
                        associativityOfUnion: Boolean,
                        distributivityOfJoinOverUnion: Boolean,
                        pullingFilterOutOfJoin: Boolean,
                        pullingAntiprojectionOutOfJoin: Boolean)

case class Conf(var partitions: Option[Int] = None,
                var inputData: List[InputData] = List(),
                var outFile: Option[String] = None,
                var onCluster: Boolean = false,
                var master: String = "local[*]",
                var localExecutor: LocalExecConf = PgSQL(),
                var comment: String = "",
                var query: String = "",
                var memPerExec: String = "40g",
                // TODO: options below should be computed automatically based on datasets stats
                var delta: Boolean = false,
                var repartitionCol: String = "", // FIXME: could be deleted (to be checked)
                var persistOnDisk: Boolean = false,
                var memoryCheckpoint: Boolean = false,
                var broadcastJoin: Boolean = true,
                var localDistinct: Boolean = false,
                var recVersion: Int = 1,
                var optimOptions: Option[OptimOptions] = None)

/**
 * Utility methods for the Spark-based demonstrators
 */
object DemoUtil {

    /**
     * Prepares the execution handlers if the demonstrators
     *
     * @param args Demonstrator command line arguments
     * @return The test configuration and the Spark executor
     */
    def prepareExecution(args: Array[String]): (Conf, SparkExecutor) = {
        val config = DemoUtil.parseArgs(args.toList, Conf())

        val localExecutorFactory = config.localExecutor match {
            case LocalWithMappings() => new LocalExecutorFactory(
                LocalExecutorConfig(true), new MappingsFactory())
            case LocalWithRows() => new LocalExecutorFactory(
                LocalExecutorConfig(true), new RowFactory())
            case PgSQL() => new PgSQLExecutorFactory(PgSQLConfig(showResult = false,
                distributed = config.onCluster))
        }

        val sparkExecutor = new SparkExecutor(false,
            onCluster = config.onCluster,
            appName = DemoUtil.getAppName(config),
            partitions = config.partitions,
            memPerExec = config.memPerExec,
            localExecutorFactory = localExecutorFactory,
            master = config.master)

        (config, sparkExecutor)
    }

    /**
     * Runs the Spark evaluation and prints out the evaluation time
     *
     * @param sparkExecutor Pre-configured Spark executor
     * @param muTerm        The Mu term to execute
     * @param recOptions    Centralized / Distributed recursion options
     */
    def runEvaluation(sparkExecutor: SparkExecutor, muTerm: Mu, recOptions: DecRecOptions): Unit = {
        val pGen = new SparkPhysicalPlanGenerator(recOptions)
        pGen.generatePhysicalPlan(muTerm)
        val startEval = System.nanoTime()
        sparkExecutor.evaluate(muTerm)
        val endEval = System.nanoTime()
        val timeMs = TimeUnit.NANOSECONDS.toMillis(endEval - startEval)
        println(s"EVAL TIME: ${timeMs / 1000f} s")
    }

    /** Command line arguments:
     *
     * file1 format1 [file2 format2...]: a list of input files or postgres tables each followed by
     * by its format (csv, postgres_table)
     *
     * Additional options:
     * --partitions nbPartitions
     *
     * --output outputFile
     *
     * --cluster: The test is run on cluster. By default the test is run locally
     *
     * --master: spark master
     *
     * --local rows or --local mappings: to set the local executor
     * to local executor with rows or local executor with mappings.
     * By default (without --local) the local executor is PgSQLExecutor
     *
     * --comment additionalComment: additional comment to put in the Spark appName
     *
     * --query queryFile: indicates the UCRPQ query to solve text file containing a UCRPQ query fixme
     *
     * -d: Perform fixpoint computations with delta
     *
     * --rc repartitionCol: index of column by which to repartition data.
     * By default data is not repartitioned.
     *
     * -l: perform only local distinct and not global distinct at the end of fixpoint computation.
     * This requires data to be repartitioned on the column that stays unchanged
     * throughout the fixpoint computations.
     *
     * -p: Spark performs a persist on disk of the fixpoint result
     *
     * --mc: memory checkpoint the intermediate results in a SetRDD recursion
     *
     * --nb: do not broadcast join for set redd recursion
     *
     * --mem memoryPerExecutor (By default 40g)
     *
     * --rv recVersion: recVersion=1 distributed loop, recVersion=2 fixpoint with SetRDD,
     * recVersion = 3 CentralLoop
     *
     * --optim: string describing the values of the optimOpts booleans (1 or 0) in order
     *
     */
    @tailrec
    def parseArgs(args: List[String], conf: Conf): Conf = {
        if (args.isEmpty) {
            return conf
        }

        val cmd = args.head
        var rest = args.tail
        cmd match {
            case "--partitions" =>
                conf.partitions = Some(rest.head.toInt)
                rest = rest.tail
            case "--query" =>
                conf.query = rest.head
                rest = rest.tail
            case "--cluster" =>
                conf.onCluster = true
            case "--master" =>
                conf.master = rest.head
                rest = rest.tail
            case "-d" =>
                conf.delta = true
            case "-l" =>
                conf.localDistinct = true
            case "-p" =>
                conf.persistOnDisk = true
            case "--mc" =>
                conf.memoryCheckpoint = true
            case "--nb" =>
                conf.broadcastJoin = false
            case "--rc" =>
                conf.repartitionCol = rest.head
                rest = rest.tail
            case "--rv" =>
                conf.recVersion = rest.head.toInt
                rest = rest.tail
            case "--output" =>
                conf.outFile = Some(rest.head)
                rest = rest.tail
            case "--comment" =>
                conf.comment = rest.head
                rest = rest.tail
            case "--optim" =>
                conf.optimOptions = Some(parseOptimOpts(rest.head))
                rest = rest.tail
            case "--mem" =>
                conf.memPerExec = rest.head
                rest = rest.tail
            case "--local" =>
                conf.localExecutor = rest.head match {
                    case "rows" => LocalWithRows()
                    case "mappings" => LocalWithMappings()
                    case _ => PgSQL()
                }
                rest = rest.tail

            case _ =>
                conf.inputData = conf.inputData :+ InputData(cmd, rest.head)
                rest = rest.tail
        }
        parseArgs(rest, conf)
    }

    def parseOptimOpts(str: String): OptimOptions = {
        val options = str.split(" ").map(o => if (o == "1") true else false).toList
        options match {
            case List(
            swapAntiprojections,
            associativityOfUnion,
            distributivityOfJoinOverUnion,
            pullingFilterOutOfJoin,
            pullingAntiprojectionOutOfJoin)
            => OptimOptions(
                swapAntiprojections,
                associativityOfUnion,
                distributivityOfJoinOverUnion,
                pullingFilterOutOfJoin,
                pullingAntiprojectionOutOfJoin)
            case _ => throw new Exception("Unexpected number of optim flags")
        }

    }

    def getAppName(conf: Conf): String = {
        val otherInfo = List(
            if (conf.repartitionCol != "") "repartition" else "",
            if (conf.localDistinct) "localdistinct" else "",
            if (conf.persistOnDisk) "persist" else "",
            if (conf.memoryCheckpoint) "memoryCheckpoint" else "",
            conf.comment
        ).filter(_ != "").mkString(sep = "+")
        println(otherInfo)
        val filename = conf.inputData.head.path.split("/").last
        "Mu Spark:%s:%s:%s:%s:%s".format(
            filename,
            conf.partitions.getOrElse(None),
            conf.localExecutor.toString,
            conf.recVersion match {
                case 1 => "distLoop"
                case 2 => "SetRDDLoop"
                case 3 => "centralSetRDDLoop"
                case 4 => "centralLoop"
            },
            otherInfo
        )
    }

    def getNameFromPath(path: String): String = path.split("/").last

    def getRecOptions(testconfig: Conf): DecRecOptions = testconfig.recVersion match {
        case 1 => DistRecOptions(delta = testconfig.delta,
            repartitionCol = testconfig.repartitionCol,
            localDistinct = testconfig.localDistinct,
            persistOnDisk = testconfig.persistOnDisk)
        case 2 => SetRDDOptions(
            repartitionCst = testconfig.repartitionCol,
            memoryCheckpoint = testconfig.memoryCheckpoint,
            broadcastJoin = testconfig.broadcastJoin)
        case 3 => CentralRecSetRDDOptions()
        case _ => CentralRecOptions(broadcastJoin = testconfig.broadcastJoin)
    }

    /**
     * Returns the best term of the explored Mu space
     *
     * @param initialTerms Initial Mu terms, generated from the parsing of a UCRPQ query
     * @param relVars       Description of the relation variables
     * @param optimOptions Term exploration and optimization options
     * @return The best Mu term found after exploration, according to the cost estimator
     */
    def bestTerm(initialTerms: Set[Mu], relVars: List[RelVar], optimOptions: OptimOptions): Mu = {
        println("optimising")

        optimOptions match {
            case OptimOptions(
            swapAntiprojections,
            associativityOfUnion,
            distributivityOfJoinOverUnion,
            pullingFilterOutOfJoin,
            pullingAntiprojectionOutOfJoin) =>
                val optimParams = new OptimizerParams(swapAntiprojections, distributivityOfJoinOverUnion,
                    associativityOfUnion, pullingFilterOutOfJoin, pullingAntiprojectionOutOfJoin)

                // Plan space exploration using MuRewrite:
                val t1 = System.currentTimeMillis()
                val exploredTerms = new MuRewriter(optimParams).topDownSearch(initialTerms, 5)
                val murewrite_time = System.currentTimeMillis() - t1
                println(s"murewrite exploration time=${murewrite_time}ms")
                println(s"Number of mu terms explored: ${exploredTerms.length}")
                // Select the best (estimated) term using a cost model for MuRewrite:
                val t3 = System.currentTimeMillis()
                // Setup the Mu cost model
                val cmSetupProps = new Properties
                cmSetupProps.setProperty("stats.normalized", "true")
                val costModel = new MuCentralizedCost
                costModel.setup(relVars, cmSetupProps)
                val muOptim = MuRewrite.selectOptimizedTerm(exploredTerms, costModel)
                val murewriteSelectionTime = System.currentTimeMillis() - t3
                println(
                    s"""Term selected by MuRewrite (${murewriteSelectionTime}ms):
                       |${MuPrinter.prettyMuIndent(muOptim)}""".stripMargin)

                //fixme for testing
                println("pushing all ops")
                new MuRewriter().pushAllOps(muOptim)
        }
    }
}
