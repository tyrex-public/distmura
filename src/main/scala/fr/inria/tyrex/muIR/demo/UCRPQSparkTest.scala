package fr.inria.tyrex.muIR.demo

import fr.inria.tyrex.muIR.core._
import fr.inria.tyrex.muIR.queries.UCRPQParser

import scala.collection.immutable
import scala.io.Source

/**
 * Computes a UCRPQ query on Spark
 *
 * Command line:
 * <pre>
 * variablePartInputGraph format --query queryFile --partitions p [--le locExec]
 * [--cluster] [--mem mem] [-d] [--rc col] [-l] [-p] [--comment c]
 * </pre>
 * (see [[DemoUtil]] for more details about those options).
 */
object UCRPQSparkTest {

    /**
     * Entry point
     *
     * @param args Command line arguments
     */
    def main(args: Array[String]): Unit = {
        val (testConfig, sparkExecutor) = DemoUtil.prepareExecution(args)

        val inFile = Source.fromFile(testConfig.query)
        try {
            val queryStr = inFile.getLines().next()
            //todo make fields as parameters to the test?
            val q = new UCRPQParser("predicate").apply(queryStr).get
            val path = testConfig.inputData.head.path
            val name = path.split("/").last.split("\\.").head
            val typ = immutable.Set(ColumnId("src"), ColumnId("predicate"), ColumnId("trg"))
            val yago = RelVar(name, typ, path, testConfig.inputData.head.format)
            val mus = Query2Mu.translate(q, yago)
            val optimise = testConfig.optimOptions.isDefined
            val baseline = if (optimise) {
                DemoUtil.bestTerm(mus, List(yago), testConfig.optimOptions.get)
            } else {
                mus.head
            }

            // Trigger the evaluation
            val recOptions = DemoUtil.getRecOptions(testConfig)
            DemoUtil.runEvaluation(sparkExecutor, baseline, recOptions)
        } finally {
            inFile.close()
        }
    }
}
