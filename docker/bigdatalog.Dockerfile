# syntax=docker/dockerfile:1
# ** First stage: building BigDatalog **
FROM openjdk:8-jdk AS builder
LABEL maintainer=thomas.calmant@inria.fr

# Download the repository
RUN git clone https://github.com/ashkapsky/BigDatalog.git /opt/BigDatalog
WORKDIR /opt/BigDatalog

# Configure Maven:
# * HTTPS Protocol is forced, else Maven repositories refuse the connection
# * Change stack size according to Spark documentation
ENV MAVEN_OPTS="-Dhttps.protocols=TLSv1.2 -Xmx2g -XX:MaxPermSize=512M -XX:ReservedCodeCacheSize=512m"

# Install the dependency
RUN build/mvn install:install-file -Dfile=datalog/lib/DeALS-0.6.jar \
        -DgroupId=DeALS -DartifactId=DeALS -Dversion=0.6 -Dpackaging=jar

# Download dependencies Maven/Java 7 can't handle
RUN wget https://repo.eclipse.org/content/repositories/paho-releases/org/eclipse/paho/org.eclipse.paho.client.mqttv3/1.0.1/org.eclipse.paho.client.mqttv3-1.0.1.jar && \
    build/mvn install:install-file -Dfile=org.eclipse.paho.client.mqttv3-1.0.1.jar \
        -DgroupId=org.eclipse.paho -DartifactId=org.eclipse.paho.client.mqttv3 \
        -Dversion=1.0.1 -Dpackaging=jar && \
    rm org.eclipse.paho.client.mqttv3-1.0.1.jar

# Copy and apply the patch to make the examples buildable
COPY docker/bigdatalog_fix_examples.patch /opt/BigDatalog/fix_examples.patch
RUN git apply /opt/BigDatalog/fix_examples.patch && \
    rm /opt/BigDatalog/fix_examples.patch

# Build Spark
RUN build/mvn -Phadoop-2.4 -Pscala-2.10 -Dhadoop.version=2.4.0 -DskipTests \
        clean install

# Make the distribution file
RUN ./make-distribution.sh --name big-datalog --tgz \
        -Phadoop-2.4 -Phive -Phive-thriftserver -Pscala-2.10

# ** Second stage: execution **
FROM openjdk:8-jdk
LABEL maintainer=thomas.calmant@inria.fr

WORKDIR /opt
COPY --from=builder "/opt/BigDatalog/spark-1.6.1-bin-big-datalog.tgz" /opt
RUN tar xzf "spark-1.6.1-bin-big-datalog.tgz" && \
    rm "spark-1.6.1-bin-big-datalog.tgz" && \
    ln -s /opt/spark-1.6.1-bin-big-datalog /opt/spark

ENV SPARK_HOME="/opt/spark"
ENV PATH="${SPARK_HOME}/bin:${PATH}" \
    BIGDATALOG_JAR="${SPARK_HOME}/lib/spark-examples-1.6.1-hadoop2.4.0.jar"

# Set the entrypoint
COPY docker/spark_entrypoint.sh /opt/spark_entrypoint.sh
RUN chmod +x /opt/spark_entrypoint.sh && sync
ENTRYPOINT [ "/opt/spark_entrypoint.sh", "bigdatalog" ]
