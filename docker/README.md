# Docker images

This folder contains Docker recipes to:
* Build DistMuRA and run it in a local Spark ([`distmura_spark.Dockerfile`](distmura_spark.Dockerfile))
* Run BigDatalog programs ([`bigdatalog.Dockerfile`](bigdatalog.Dockerfile))

**Note:** Run all the given commands at the root of this project, *i.e.* the folder above the one
containing **this** file.

## Requirements

These images have been compiled with Docker 20.10.8.

As the `distmura_spark.Dockerfile` uses the `COPY` instruction and that the `docker run` commands
use volumes, you might want to check if your system is correctly configured for that.
A rule of thumb is to check from this directory if the command
`docker run --rm -v ${PWD}:/host debian bash -c "echo working >> /host/docker_test.txt"`
successfully writes down the `docker_test.txt` in the current working directory.

## Sample dataset

In order to test the Docker images, you can use the `uniprot_mini.csv` dataset, available in this
repository.
Note that this dataset wasn't used in our paper as it is only used to test the infrastructure.

## Run DistMuRA with Spark in Docker

1. Move to the `muir-distmura-public` folder
2. Ensure that it contains the `muIR-assembly-0.1.0-SNAPSHOT.jar` file
3. Run the following command to generate the `distmura-spark:latest` image
   ```bash
   docker build -t distmura-spark:latest -f docker/distmura_spark.Dockerfile .
   ```
   The image compilation should take around 3&nbsp;minutes.
   In this image, the DistMuRA JAR file can be found as `/opt/distmura-demo-assembly.jar`, as
   indicated by the `$DISTMURA_JAR` environment variable.
4. Run the Docker container in interactive mode and opening the Spark Worker UI port
   ```bash
   docker run -it -v ${PWD}:/host -w /host -p 4040:4040 distmura-spark
   ```
   Note that the Spark Worker UI is available only during the execution of the Spark job.

### Adaptation of the commands given in the root [README](../README.md)

1. Replace all instances of `muir-distmura-public-assembly-0.1.0-SNAPSHOT.jar` by `$DISTMURA_JAR`
2. Remove the `--cluster` argument
3. Update both the `--driver-memory` and `--mem` arguments to match the capabilities of your computer

Note that the `MASTER_URL` environment variable is set in the Dockerfile to be `local[*]`.
The `NB_PARTITIONS` variable is computed when starting the Docker container to match the number
of cores of the first CPU of the computer.

### Sample run

Here is an example session to run the UCRPQ program `Q1.txt` with the `uniprot_1M.csv` dataset

1. Run the container:
   ```bash
   docker run -it -v ${PWD}:/host -w /host -p 4040:4040 distmura-spark
   ```
2. Run the following command inside the container:
   ```bash
   DATA_FILE=datasets/distmura_format/uniprot_mini.csv
   QUERY_PATH=queries/ucrpq_queries/Q1.txt

   spark-submit \
      --class fr.inria.tyrex.muIR.demo.UCRPQSparkTest \
      --driver-memory 1g \
      --conf spark.driver.maxResultSize=0 \
      $DISTMURA_JAR \
      $DATA_FILE \
      csv \
      --mem 1g \
      --master $MASTER_URL \
      --partitions $NB_PARTITIONS \
      --query $QUERY_PATH \
      --rv 2 \
      --optim "1 1 1 0 0" \
      --mc
   ```
   During the execution, you can access the Spark Worker UI at <http://localhost:4040/>
3. The time taken to evaluate the query is printed out at the end of the execution, prefixed with
   `EVAL TIME:`

## Run BigDatalog programs in Docker

1. Compile the `distmura-bigdatalog` Docker image
   ```bash
   docker build -t distmura-bigdatalog:latest -f docker/bigdatalog.Dockerfile .
   ```
   The image takes around 50&nbsp;minutes to compile.
2. Run the container:
   ```bash
   docker run -it -v ${PWD}:/host -w /host -p 4040:4040 distmura-bigdatalog
   ```
3. Run the following command inside the container:
   ```bash
   DATA_FILE=datasets/bigdatalog_format/uniprot_mini.csv
   PROGRAM_FILE=queries/datalog_query_programs/q1.deal

   spark-submit \
      --driver-memory 40g \
      --executor-memory 40g \
      --master $MASTER_URL \
      --conf spark.datalog.recursion.version=2 \
      --conf spark.datalog.uniondistinct.enabled=true \
      --conf spark.sql.shuffle.partitions=$NB_PARTITIONS \
      --conf spark.driver.maxResultSize=0 \
      --class org.apache.spark.examples.datalog.Experiments \
      $BIGDATALOG_JAR \
      --file=$(p=$PROGRAM_FILE; echo $p > ${p}.txt; echo ${p}.txt) \
      --queryform=$(grep "<-" $PROGRAM_FILE -m 1 | cut -d"<" -f1) \
      --program=99 \
      --checkpointdir="$(pwd)/tmp" \
      --baserelation_yago=$DATA_FILE \
      --baserelation_yagodup=$DATA_FILE \
      --baserelation_yagodupdup=$DATA_FILE
    ```
   During the execution, you can access the Spark Worker UI at <http://localhost:4040/>
4. The time taken to evaluate the query is printed out at the end of the execution, prefixed with
   `execution time:`

**Note:** The docker image was tested with commit f7809840cc3c5ad3ed67db480f2ed7ed2b9ac2e2 of
the [BigDatalog](https://github.com/ashkapsky/BigDatalog/) repository.
