FROM debian
LABEL maintainer=thomas.calmant@inria.fr

# Install the survival kit
RUN apt update && \
    apt install -y wget ca-certificates

# Install Temurin
WORKDIR /opt
RUN wget -O- openjdk8.tgz https://github.com/adoptium/temurin8-binaries/releases/download/jdk8u312-b07/OpenJDK8U-jdk_x64_linux_hotspot_8u312b07.tar.gz | tar xz && \
    mv /opt/jdk8u* /opt/jdk8

ENV PATH=/opt/jdk8/bin:$PATH \
    JAVA_HOME=/opt/jdk8

# Install Maven
RUN wget -O- https://archive.apache.org/dist/maven/maven-3/3.8.3/binaries/apache-maven-3.8.3-bin.tar.gz | tar xz && \
    mv /opt/apache-maven-* /opt/maven

ENV PATH=/opt/maven/bin:$PATH \
    MAVEN_HOME=/opt/maven \
    M2_HOME=/opt/maven \
    M3_HOME=/opt/maven

# Install sbt
RUN wget -O- https://github.com/sbt/sbt/releases/download/v1.5.5/sbt-1.5.5.tgz | tar xz
ENV PATH=/opt/sbt/bin:$PATH

# Download Spark
WORKDIR /opt
RUN wget -O- https://archive.apache.org/dist/spark/spark-2.4.5/spark-2.4.5-bin-hadoop2.7.tgz | tar xz && \
    mv /opt/spark* /opt/spark

ENV PATH=/opt/spark/bin:$PATH

# Compile the assembly JAR
COPY . /opt/distmura
WORKDIR /opt/distmura
RUN mvn install:install-file -Dfile="muIR-assembly-0.1.0-SNAPSHOT.jar" \
           -DgroupId="fr.inria.tyrex" \
           -DartifactId="muir-core" \
           -Dversion="0.1.0-distmura" \
           -Dpackaging=jar && \
    sbt assembly && \
    mv target/scala-2.11/muir-distmura-public-assembly-0.1.0-SNAPSHOT.jar /opt/distmura-demo-assembly.jar

ENV DISTMURA_JAR=/opt/distmura-demo-assembly.jar

# Set the entrypoint
COPY docker/spark_entrypoint.sh /opt/spark_entrypoint.sh
RUN chmod +x /opt/spark_entrypoint.sh && sync
ENTRYPOINT [ "/opt/spark_entrypoint.sh", "distmura" ]
