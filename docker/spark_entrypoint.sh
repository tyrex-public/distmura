#!/bin/bash

export MASTER_URL=${MASTER_URL:-'local[*]'}
export NB_PARTITIONS=${NB_PARTITIONS:-$(grep ^cpu\\scores /proc/cpuinfo | uniq |  awk '{print $4}')}

echo "#############################################################"
echo "# DO NOT FORGET TO SET THE FOLLOWING ENVIRONMENT VARIABLES: #"
echo "#                                                           #"
echo "# * DATA_FILE: Path to the dataset                          #"
echo "# * START_NODE: Start vertex ID (used for the Reach query)  #"

if [ "$1" == "distmura" ]
then
    echo "# * QUERY_PATH: Path to the query file                      #"
    echo "# * START_NODE: ID of the starting node in the Reach query  #"
elif [ "$1" == "bigdatalog" ]
then
    echo "# * PROGRAM_FILE: Path to the BigDatalog .deal program file #"
else
    echo "! UNKNOWN RUN ENVIRONMENT                                   !"
fi
echo "#############################################################"

bash
exit $?
